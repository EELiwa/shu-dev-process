// modified from MDN docs: https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers

const VERSION = `0.3.0`;
const CACHE_NAME = `shu-dev-astro-sw-${VERSION}`;

//these are files that are never expected to change
const base = "https://aserg.codeberg.page/shu-dev-process";
const alwaysCached = [
  base,
  `${base}/en/introduction`,
  `${base}/favicon.ico`,
  `${base}/fonts/FiraCode-variableFont.ttf`,
  `${base}/search_docs.json`,
];

const addResourcesToCache = async (resources) => {
  try {
    const cache = await caches.open(CACHE_NAME);
    await cache.addAll(resources);
  } catch (e) {
    console.warn("Some resources could not be added to the cache: ", e);
  }
};

const putInCache = async (request, response) => {
  if (request.method == "GET") {
    const cache = await caches.open(CACHE_NAME);
    await cache.put(request, response);
  }
};

const fromCache = async (request) => {
  const responseFromCache = await caches.match(request);
  if (responseFromCache) {
    return responseFromCache;
  }
  return null;
};

const fromPreload = async (preloadResponsePromise) => {
  const preloadResponse = await preloadResponsePromise;
  if (preloadResponse) {
    console.info("using preload response", preloadResponse);
    putInCache(request, preloadResponse.clone());
    return preloadResponse;
  }
  return null;
};

const fromNetwork = async (request, fallbackUrl) => {
  try {
    const responseFromNetwork = await fetch(request);
    // response may be used only once
    // we need to save clone to put one copy in cache
    // and serve second one
    putInCache(request, responseFromNetwork.clone());
    return responseFromNetwork;
  } catch (error) {
    if (fallbackUrl) {
      try {
        const fallbackResponse = await caches.match(fallbackUrl);
        if (fallbackResponse) {
          return fallbackResponse;
        }
      } catch (e) {
        console.warn(e);
      }
    }
    console.warn(error);
    return null;
  }
};

const networkErrorResponse = (msg = "", httpCode = 408) => {
  // when even the fallback response is not available,
  // there is nothing we can do, but we must always
  // return a Response object
  return new Response(
    msg ??
      "Network error. Perhaps you are not connected to the internet? If this issue persists, please contact the ASERG team.",
    {
      status: httpCode,
      headers: { "Content-Type": "text/plain" },
    }
  );
};

const cacheFirst = async ({ request, preloadResponsePromise, fallbackUrl }) => {
  return (
    fromCache(request) ??
    fromPreload(preloadResponsePromise) ??
    fromNetwork(request, fallbackUrl) ??
    networkErrorResponse()
  );
};

const networkFirst = async ({
  request,
  preloadResponsePromise,
  fallbackUrl,
}) => {
  return (
    fromPreload(preloadResponsePromise) ??
    fromNetwork(request, fallbackUrl) ??
    fromCache(request) ??
    networkErrorResponse()
  );
};

// Enable navigation preload
const enableNavigationPreload = async () => {
  if (self.registration.navigationPreload) {
    await self.registration.navigationPreload.enable();
  }
};

const deleteCache = async (key) => {
  await caches.delete(key);
};

const deleteOldCaches = async () => {
  const cacheKeepList = [CACHE_NAME];
  const keyList = await caches.keys();
  const cachesToDelete = keyList.filter((key) => !cacheKeepList.includes(key));
  await Promise.all(cachesToDelete.map(deleteCache));
};

self.addEventListener("activate", (event) => {
  event.waitUntil(async () => {
    await enableNavigationPreload();
    await deleteOldCaches();
  });
});

self.addEventListener("install", (event) => {
  event.waitUntil(addResourcesToCache(alwaysCached));
});

self.addEventListener("fetch", (event) => {
  event.respondWith(
    networkFirst({
      request: event.request,
      preloadResponsePromise: event.preloadResponse,
      fallbackUrl: "",
    })
  );
});
