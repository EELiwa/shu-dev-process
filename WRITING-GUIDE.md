# Writing Guide

This document outlines page and section level guidance for content on the shu-dev site. We want students to use the site so the aim of this document is to provide a coherent and accessible voice for the content by standardising what they read. Remember, the site is not going to be read as formal, technical documentation (though sections of it may get close), but as an aid for student learning. Write as if you're talking to a human rather than an expert or a robot.

The guide has been written by the research team with influences from [Teach, don't tell](https://stevelosh.com/blog/2013/09/teach-dont-tell/#s10-the-anatomy-of-good-documentation), [Dealing with Technical or Professional Jargon](https://www.nngroup.com/articles/technical-jargon/) and [Writing So Your Words are Read](https://www.youtube.com/watch?v=8LiV759Bje0).

## Personas 

Shu-dev readers aren't you. Students have a multitude of different backgrounds and experiences around how they use computers, even before they start to write code and develop software. They may have no familiarity or understanding of the topic you're presenting before they read the website. The personas below provide only a few examples of the type of readers you may encounter.

| **Name** 	| **Biography** 	| **Considerations** 	|
|---	|---	|---	|
| Jack 	| A first year mature student who takes time to understand technical terms, but they make good use of them once they're familiar. 	| How can technical terms be replaced for level 4 students? Should they only be introduced at level 5?	|
| John 	| A second year student with dyslexia and no limited knowledge of your topic. 	| Is there a simpler alternative to the words you've used? 	|
| Jane 	| Has blurred vision and color blindness. 	| Can your diagrams and figures use greater contrast and less text? How can you caption images in order to describe their content? 	|
| Julie 	| A third-year student with English as her second language. She struggles understanding some everyday language. 	| Have you used plain language? If it's uncommon, how can it be explained? 	|

## Easy Reading and using Jargon

### Easy Reading

According to Nielson Norman, [all readers prefer plain lanuage](https://www.nngroup.com/articles/plain-language-experts/). It is especially important for helping students with learning difficulties so here are a number of tips you can use when writing to keep your content accessible to everyone:

- Use **short sentences** so readers can commit them to memory. Have one idea or action per sentence.
- Use **plain language** (see jargon details below).
    - You might wish to provide more detailed explanations only to more experienced students. See 'Filtering Content' below.
    - Be **consistent**. For example, don't switch between 'IDE' and 'code editor',
- Use [**gender-neutral language** (techwirl)](https://techwhirl.com/gender-neutral-technical-writing/). Eliminate pronouns where possible or maintain a mixture if they cannot be avoided.
- **Make your content easy to skim**, so that readers can find the information they need and pause between sections:
    - Use lots of **headings** to organise topics
    - Use bullet points for lists
    - Emphasise keywords so readers using double astericks in markdown so that `**this text**` becomes **this text**.
- Use **pictures and diagrams** to explain cocepts in a visual way. Make sure there is **high contrast** and they can be understood without colour (using **shapes** and **patterns**).


These tips have been summarised from the guides below, though plenty more exist online.

- [Learning Disability Communication Guidelines - NHS](https://www.england.nhs.uk/wp-content/uploads/2018/06/LearningDisabilityAccessCommsGuidance.pdf)
- [Creating Accessible Documents - AbilityNet](https://abilitynet.org.uk/factsheets/creating-accessible-documents-0)
- [Plain Language is for Everyone - NN](https://www.nngroup.com/articles/plain-language-experts/)

### Jargon

Words and terms that are obvious to you might not be obvious to others. However, the technical nature of our subject area means there are times when they cannot be avoided. [Dealing with Technical or Professional Jargon](https://www.nngroup.com/articles/technical-jargon/) by Neilson Norman describes how jargon affects user experience and a useful method to decide when to use it. Seen below, it can be applied to the SHU Development Process with the following considerations:

1. New concepts and technologies will be unfamiliar to all students when they are first introduced. This is especially true for foundation or level 4 students where prior knowledge cannot be assumed. Always use an explanation for the first mention of a term.
2. Terms familiar to one course may not be familiar to another. Students with an interest or modules related to web technologies may not be familiar with jargon related to graphics processing, and vice versa. For this documentation, consider both the applicability of a term across the computing department and it's introduction in prior levels of study before using it without an explanation.
3. When a term isn't important to the core learning of a page but a plain-language alternative cannot be used, you should provide a link that explains it. Where possible, these should be internal links to another shu-dev page, otherwise reputable third-parties should be used.

![Jargon Decision Tree from Neilson Norman](repo-images/jargon-decision.png)

### Online Tools

Software like [ProWritingAid](https://prowritingaid.com/), [Grammarly](grammarly.com), [HemingwayApp](https://hemingwayapp.com/), and even [Word](https://support.microsoft.com/en-au/office/get-your-document-s-readability-and-level-statistics-85b4969e-e80a-4777-8dd3-f7fc3c8b3fd2) can provide scores and suggestions for the reading level of your writing. They are informed by sentence length, syllable counts, and the total number of words. The broad ability range of students means that shu-dev doesn't target a specific reading level or require implementation of every suggestion, but they can be helpful for understanding changes that improve accessibility.

![A screenshot of the writing suggestions on hemingwayapp.com](repo-images/hemingway.png)
*A screenshot of the writing suggestions on hemingwayapp.com*

## Providing Examples

//todo: give context to the consistent case study and example #59 that will form the basis of examples in future
//todo: explain gender neutral language
// https://techwhirl.com/gender-neutral-technical-writing/

## Syntax Considerations

The pages use [`.mdx`](https://mdxjs.com/), which is an extension of [Markdown](https://daringfireball.net/projects/markdown/). Among other features, it allows the use of HTML components within text documents. For this project you can use it like regular markdown, for which full information can be found on [markdownguide.org](https://www.markdownguide.org/).

### Filtering Content

Students may want to know which elements of a page to pay attention to for their level of study, even if they aren't using the filter controls (see 'Filtering Content' below). For this reason, try to include a 'What is Expected' section at the top of your document. See existing documents for examples.

Theme support has been added for filtering content based on level of study. This means you can [progressively disclose]() Here's how you can make use of it:

1. Ensure you are using an `.mdx` file. If you're using a regular `.md` file, you can simply update the file extension.
2. Add `import FilterContent from "@contentFilter";` below the frontmatter.
3. Fence the content you want to filter in a `<FilterContent/>` element.
   1. Add an `options` attribute containing the levels of study that should see your content. These are `4`,`5`, & `6`.
   2. Multiple levels should be joined with a comma.
   3. Include all levels that should see your content. 
   
**Note:** Level 4 is the first level of study and content at this level is likely to apply in every subsequent year, so there may be no need to filter it at all.

*The controls for filtering are configured in `src/config.ts`.*

![The appearance of content with a filter is distinguished by a text label and border colors](repo-images/level-filters.png)
*Content with a filter is distinguished by a text label and colored borders.*

**Example**

*The `block` attribute can be used to conveniently set `display:block; on the container.*

*`className` can be set for additional CSS styling.*

```
---
title: User Stories
description: A User Story is an informal, natural language description of one or more features of a software system.
---
import FilterContent from "@contentFilter";

<FilterContent options="5,6" block className="class1 class2">
    //some content for second year and beyond
</FilterContent>
```

### Embedding Content

#### Other Pages

Links to other pages should include the full pathname for the url that includes the language code:

```markdown
[A link to modelling analysis](/en/modelling/analysis)
```

#### Images, Figures, and Diagrams

To help every reader understand your visual media, you can do the following:

- **Uses contrasting colors** - these can be checked with a tool like [contrast checker](https://webaim.org/resources/contrastchecker/).
- **Use icons, shapes, patterns (for filling shapes) and labels** - so that elements can be distinguished and understood without color.
- **Clear labels and captions** - Significant parts of your diagram should be described with clear text, which may take the form of titles, labels, or chart keys. The caption is especially important for people using screen readers.  

##### Adding captions in Markdown

```markdown
![A description of the image used in the alternative text](https://aserg.codeberg.page/shu-dev-process/some-image.png)
*This caption appears below the image in italic text. It should concisely describe the content of the image.*
```

#### Mermaid Diagrams

To use [mermaid diagrams](https://mermaid-js.github.io/mermaid/#/) in content, simply set the language of a code fence to `mermaid`.

~~~
```mermaid
    //your code here
```
~~~

# Page Structure

The major sections and metadata should be consistent across the website.

## Major Sections

- **Summary** - Generated using Frontmatter (below).
- **What is Expected** - Used to tell students what they should take away from the page given their level of study. Take advantage of the controls in 'Filtering Content' above.
- **[Your Content]** - The content should be ordered approximately by level of study or the order in which it should be learned, such that someone reading the page for the first time could work through all of it to build up what they know. Your content needs to help them build familiarity.
- **References** - Include sources for your information here. They currently use a square numbered format. E.g: `[1]`. 
- **Further Reading** - Optional. Generated using Frontmatter (below).

## Frontmatter Metadata

Frontmatter is the metadata that appears at the top of markdown files. It can be used to set the following attributes:

- `title: string` - The page title displayed in browser tabs and before content. All pages should include this.
- `description: string` - Used for SEO and to display a summary of the content of the page. Plaintext only.
- `furtherReading?: string` - Optional. Displayed at the end of a page to highlight related topics that haven't been explicitly covered, but that may still be of interest to students.
- `draft?: boolean` - Optional. Mark a page as a draft to let readers know it isn't finished or that it may change. Draft pages don't appear in search results.

## Template

[`PAGE-TEMPLATE.mdx`](PAGE-TEMPLATE.mdx) is a file you can copy for your own pages. It contains examples of every section in this guide.