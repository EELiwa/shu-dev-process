import { test, expect } from "@playwright/test";
import { expectResponseIsOk, getURLs } from "../util.ts";

const [firstURL, secondURL] = getURLs(2);

test("stores value and doesn't reappear", async ({ page }) => {
  //Go to first URL
  const resp1 = await page.goto(firstURL);
  expectResponseIsOk(resp1);
  //click cookie privacy bttn
  let banner = await page.locator("#privacy-banner");
  await banner.locator("button").click();
  await expect(banner, `banner should be dismissed`).toHaveCSS(
    "display",
    "none"
  );
  const storedValue = await page.evaluate(() =>
    localStorage.getItem("privacy-accepted")
  );
  expect(storedValue, "cookie acceptance should be stored").not.toBeNull();
  //go to another page
  const resp2 = await page.goto(secondURL);
  expectResponseIsOk(resp2);
  banner = await page.locator("#privacy-banner");
  await expect(banner, `banner should not be displayed`).toHaveCSS(
    "display",
    "none"
  );
});
