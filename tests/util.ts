import { SIDEBAR } from "../src/config.ts";
import { expect } from "@playwright/test";
import Sitemapper from "sitemapper";

export const IS_DEV = process.env.NODE_ENV == "devdeploy";
export const SITE = IS_DEV
  ? "http://localhost:3000/shu-dev-process/"
  : "https://aserg.codeberg.page/shu-dev-process/";

export function getURLs(limit: number = 0): string[] {
  let urls = [];
  const underLimit = () =>
    !limit ? true : limit <= 0 ? true : urls.length < limit;
  Object.entries(SIDEBAR["en"]).forEach(([sectionName, links], idx) => {
    links.forEach(({ text, link, subheading }) => {
      if (link && underLimit()) {
        //remove fragments from links
        link = link.trim().replaceAll(new RegExp("#.*$", "gmi"), "");
        urls.push(link);
      }
    });
  });
  if (underLimit()) {
    let sitemapper = new Sitemapper({ timeout: 15000 });
    sitemapper.fetch(`${SITE}sitemap-index.xml`).then((result) => {
      result.sites.forEach((link) => {
        if (underLimit()) {
          link = link.replace(SITE, "");
          urls.push(link);
        }
      });
    });
  }
  const result = Array.from(new Set(urls));
  return result;
}

export async function expectResponseIsOk(resp): Promise<any> {
  //MakeMaker return type is part of Playwright, but cannot be imported
  return await expect(
    resp.ok(),
    `${resp.status()} ${resp.statusText()}`
  ).toBeTruthy();
}
