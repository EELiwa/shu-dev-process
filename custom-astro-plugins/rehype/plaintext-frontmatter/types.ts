import type { HastNode } from "hast-util-to-text";

// from withastro/astro/packages/astro-rss/src/index.ts
export type GlobResult = Record<string, () => Promise<{ [key: string]: any }>>;

export interface AstroVFile {
  data: { astro: { frontmatter: Record<string, any> } };
}

export type AstroRehypePlugin = (tree: HastNode, file: AstroVFile) => void;

export type PlaintextFrontmatterPluginOptions = {
  /** Frontmatter property to store plain text output, default "plainText". */
  contentKey?: string;

  /** If true, strip emoji out of text */
  removeEmoji?: boolean;

  /** Tags to consider headings and make separate search documents.
   * Default = ["h1","h2", "h3","h4","h5","h6"]
   */
  headingTags?: string[]; 
};

export type SearchDocument = {
  url?: string;
  heading?: string;
  title: string;
  text: string;
};

// internal type for assembling document sections
export type Section = { heading: string; text: string};

