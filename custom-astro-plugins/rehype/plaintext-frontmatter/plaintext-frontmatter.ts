import { HastNode, HastElement, toText } from "hast-util-to-text";
import { visit } from "unist-util-visit";
import EmojiRegex from "emoji-regex";

import {
  AstroRehypePlugin,
  GlobResult,
  PlaintextFrontmatterPluginOptions,
  SearchDocument,
  Section,
} from "./types";

const defaultOptions = {
  contentKey: "minisearch",
  removeEmoji: false,
  headingTags: ["h1", "h2", "h3", "h4", "h5", "h6"],
};

const emojiRegex = EmojiRegex();

export async function mapGlobResult(
  items: GlobResult,
  options?: PlaintextFrontmatterPluginOptions
): Promise<SearchDocument[]> {
  const documents: SearchDocument[] = [];
  const opts: PlaintextFrontmatterPluginOptions = { ...defaultOptions, ...options };
  const contentKey = opts.contentKey || defaultOptions.contentKey;

  await Promise.all(
    Object.values(items).map(async (getInfo,idx,array) => {
      const { url, frontmatter } = await getInfo();

      // Make sure the result of glob is the right shape
      if (!url) throw new Error(`missing url: ${array[idx]}`);
      if (!frontmatter) console.error("missing frontmatter", array[idx]);

      const { [contentKey]: textData, ...other } = frontmatter ?? {};

      //document fragments won't appear in the site search
      if(other?.fragment){
        return;
      }

      let sections: Section[] = [];

      if (!textData) {
        return;
      } else if (typeof textData === "string") {
        // if plain text was a simple string, treat like an object with null key
        sections = [{ heading: "", text: textData }];
      } else if (Array.isArray(textData)) {
        sections = textData.map(([heading, text, adf]) => {
          return { heading, text, adf };
        });
      }

      const newDocs: SearchDocument[] = sections.map(({ heading, text, adf }) => {
        return { url, heading, text, adf, ...other };
      });
      documents.push(...newDocs);
    })
  );
  return documents;
}

/* As of Astro v1.4.0, rehype plugins do not yet have a document tree with headings that have IDs. 
  As far as we're concerned, the internal function rehypeCollectHeadings (in 
  astro/packages/markdown/remark/src/rehype-collect-headings.ts) has not run yet.
*/

export function toPlaintextTree(
  tree: HastNode,
  options: PlaintextFrontmatterPluginOptions
): string[][] | string {
  const headingTags = options.headingTags || [];
  const sections: Section[] = [];
  const spaceRegex = /\s\s+/g;

  let section: Section = { heading: "", text: "" };

  function addSection() {
    let { heading, text } = section;

    if (options.removeEmoji) {
      heading = heading.replace(emojiRegex, "");
      text = text.replace(emojiRegex, "");
    }

    sections.push({
      heading: heading.replace(spaceRegex, " ").trim(),
      text: text.replace(spaceRegex, " ").trim()
    });
  }

  visit(tree, ["element", "text"], (node) => {
    const excluded = ["svg", "path", "g", "line", "rect", "script", "style", "astro-island", "nav", "video", "code", "pre", "kbd", "fig", "figcation"];
    if (node.type === "element") {
      const el = node as HastElement;
      const is_excluded = excluded.includes(el.tagName.toLowerCase());
      if (is_excluded) {
        return;
      }
      //skip elements that shouldn't be indexed
      if (headingTags.includes(el.tagName) || node?.properties?.role == "img") {
        let heading = toText(node);
        if (!heading) return;
        addSection(); // add current section before starting another
        section = { heading, text: "" };
      }
    } else if (node.type === "text") {
      const text = toText(node);
      if (section.text.length > 0 || section.heading !== text) {
        section.text += text + " ";
      }
    }
  });
  addSection(); // add the last section

  const output = sections
    .filter((s) => s.text.length > 0)
    .map((s) => [s.heading, s.text]);

  // output text if only one section, no heading
  return output.length === 1 && output[0][0] === "" ? output[0][1] : output;
}

export function plaintextFrontmatter(options?: PlaintextFrontmatterPluginOptions) {
  const opts: PlaintextFrontmatterPluginOptions = { ...defaultOptions, ...options };
  const contentKey = opts.contentKey || defaultOptions.contentKey;

  return function plugin(): AstroRehypePlugin {
    return (tree: HastNode, { data }) => {
      if (!data || !data.astro || !data.astro.frontmatter) return;
      const frontmatter = data.astro.frontmatter;
      if (frontmatter[contentKey] === undefined) {
        frontmatter[contentKey] = toPlaintextTree(tree, opts);
      }
    };
  };
}