import { HastNode, HastElement } from "hast-util-to-text";
import { visit } from "unist-util-visit";

interface AstroVFile {
    data: { astro: { frontmatter: Record<string, any> } };
}

type RehypePlugin = (tree: HastNode, file: AstroVFile) => void;

export default function makeCodeFocusable(): RehypePlugin {
        return (tree: HastNode, { data }) => {
            visit(tree, 'element', function (node: HastElement) {
                if (node.tagName == "pre" && Object.keys(node?.properties).indexOf("tabindex") == -1) {
                    node.properties.tabindex = '0';
                }
            });
        }
    }