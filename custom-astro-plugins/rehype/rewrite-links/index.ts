import { HastNode, HastElement } from "hast-util-to-text";
import { visit } from "unist-util-visit";
import { RewriteLinksPluginOptions, AstroRehypePlugin } from "./types";
import { bgGreen, black, cyan, green, grey } from "kleur/colors";
import readline from "readline";

//TODO: make logging neater
const default_log_dict = {
  public: 0,
  prefix: 0,
  blank: 0,
  trailingSlash: 0,
  extensions: {},
  transforms: {},
};
let log_dict = {
  public: 0,
  prefix: 0,
  blank: 0,
  trailingSlash: 0,
  extensions: {},
  transforms: {},
};

function modify(
  node: HastElement,
  prop: "href" | "src",
  options?: RewriteLinksPluginOptions
) {
  // only apply any changes if there are options configured,
  // the node has this property
  const has_prop = Object.keys(node.properties).indexOf(prop) != -1;
  if (options && options.origin && has_prop) {
    const propertyStr = "" + node.properties[prop];
    const is_fragment = propertyStr.startsWith("#");
    const hasHTTP = new RegExp("^https?", "gmi").test(propertyStr);
    const is_relative = !hasHTTP || propertyStr.startsWith("/");
    const optionURL = new URL(options.origin); //convert to a URL in case the options contain a full URL, so it can be truncated properly
    const optionsOrigin = optionURL.origin;
    var url = new URL(propertyStr, is_relative ? optionsOrigin : undefined);
    const optionsPrefix = options.prefix
      ? options.prefix.replace(/\/$/gm, "")
      : "";
    const optionsHost = optionURL.host;
    const is_external = url != null && url.host != optionsHost;

    if (!is_fragment && !is_external) {
      if (is_relative) {
        //remove '.md(x)' file extension from link pathnames, unless they're in the 'public' or 'files' folder
        [".md", ".mdx"].forEach((ext) => {
          if (
            options?.fixMd &&
            url.pathname.indexOf("/public/") == -1 &&
            url.pathname.indexOf("/files/") == -1 &&
            url.pathname.endsWith(ext)
          ) {
            url.pathname = url.pathname.substring(
              0,
              url.pathname.length - ext.length
            );
            if (options.logging) {
              if (Object.keys(log_dict.extensions).indexOf(ext) == -1) {
                log_dict.extensions[ext] = 1;
              } else {
                log_dict.extensions[ext] += 1;
              }
            }
          }
        });

        //remove /public from the start of URLs
        if (options?.removePublic && url.pathname.startsWith("/public")) {
          url.pathname = url.pathname.substring(0, 7);
          if (options.logging) {
            log_dict.public += 1;
          }
        }
      }

      //ensure internal links maintain the site path base
      if (optionsPrefix && !url.pathname.startsWith(optionsPrefix)) {
        url.pathname = optionsPrefix + url.pathname;
        if (options.logging) {
          log_dict.prefix += 1;
        }
      }

      //add target=_blank to external links
      if (prop == "href") {
        let is_anchor = node.tagName == "a";
        let missing_target =
          Object.keys(node.properties).indexOf("target") == -1;
        if (
          options?.addBlankTarget &&
          is_anchor &&
          is_external &&
          missing_target
        ) {
          node.properties.target = "_blank";
          if (options.logging) {
            log_dict.blank += 1;
          }
        }
      }

      //ensure page links end with a trailing slash
      const endsWithSlash = !url.pathname.endsWith("/");
      const isPublic = url.pathname.indexOf("/public/") == -1;
      const isFile = url.pathname.indexOf("/files/") == -1;
      const hasFileExt = url.pathname.match(
        new RegExp(".[A-z0-9]{2,3}$", "gmi")
      );
      if (
        options?.addTrailingSlash &&
        !endsWithSlash &&
        !isPublic &&
        !isFile &&
        !hasFileExt
      ) {
        url.pathname += "/";
        if (options.logging) {
          log_dict.trailingSlash += 1;
        }
      }
    }

    //allow custom transforms to be run on any link
    if (options?.transforms?.length) {
      options.transforms.forEach((fn, idx) => {
        fn(node, prop);
        if (options.logging) {
          if (Object.keys(log_dict.extensions).indexOf(`${idx}`) == -1) {
            log_dict.extensions[`${idx}`] = 1;
          } else {
            log_dict.extensions[`${idx}`] += 1;
          }
        }
      });
    }

    node.properties[prop] = url.toString();
  }
}

let logHasBeenCalled = false;
function logHeaderMessage() {
  if (!logHasBeenCalled) {
    console.log(
      logHasBeenCalled ? "" : `\n${bgGreen(black(" rewriting links "))}`
    );
    logHasBeenCalled = true;
  }
}
function getLogLines() {
  const publicStr = log_dict.public
    ? `/public removed from ${log_dict.public} links`
    : "";
  const prefixStr = log_dict.prefix
    ? `prefix added to ${log_dict.prefix} links`
    : "";
  const blankStr = log_dict.blank
    ? `target _blank added to ${log_dict.blank} links`
    : "";
  const slashStr = log_dict.trailingSlash
    ? `trailing '/' added to ${log_dict.blank} links`
    : "";
  const extensionsStr = Object.keys(log_dict.extensions).length
    ? "file extensions changed: " +
      Object.entries(log_dict.extensions)
        .map(([ext, count]) => {
          return `${ext}: ${count}`;
        })
        .join(", ")
    : "";
  const transformsStr = Object.keys(log_dict.transforms).length
    ? "transforms run:" +
      Object.entries(log_dict.extensions)
        .map(([name, count]) => {
          return `${name}: ${count}`;
        })
        .join(", ")
    : "";

  return [
    publicStr,
    prefixStr,
    blankStr,
    slashStr,
    extensionsStr,
    transformsStr,
  ]
    .map((x) => x.trim())
    .filter((x) => x.length);
}

function outputFileLog(options, file) {
  if (options?.logging) {
    const linesToPrint = getLogLines();
    logHeaderMessage();
    const cwd = (file.cwd + "").replaceAll("\\", "/");
    const relpath = (file.history + "")
      .replaceAll("\\", "/")
      .replaceAll(cwd, "");
    console.log(`${green("▶")} ${relpath}`);
    if (linesToPrint.length) {
      for (let i = 0; i < linesToPrint.length; ++i) {
        const char = i == linesToPrint.length - 1 ? "  └" : "  ├";
        console.log(`${cyan(`${char}─ `)}${grey(linesToPrint[i])}`);
      }
    } else {
      console.log(`${cyan("  └─ ")} ${grey("no changes made")}`);
    }
  }
}

export type { RewriteLinksPluginOptions };

export default function rewriteLinks(options?: RewriteLinksPluginOptions) {
  //plugin() is called for every page
  return function plugin(): AstroRehypePlugin {
    return (tree: HastNode, file) => {
      if (options && options.origin) {
        log_dict = JSON.parse(JSON.stringify(default_log_dict));
        visit(tree, "element", function (node: HastElement, idx, parent) {
          modify(node, "href", options);
          modify(node, "src", options);
        });
        outputFileLog(options, file);
      }
    };
  };
}
