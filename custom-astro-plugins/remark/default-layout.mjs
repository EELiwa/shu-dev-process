// taken from the RFC in Astro: https://github.com/withastro/astro/issues/397

import { existsSync } from "fs";
import { join } from "path";

const pagesPath = "src/pages";
const layoutPath = "src/layouts";
const layoutAlias = "@layouts";
const defaultLayout = "MainLayout";

const pascalCache = {};
function toPascalCase(str) {
  pascalCache[str] =
    pascalCache[str] ||
    (/^[\p{L}\d]+$/iu.test(str) &&
      str.charAt(0).toUpperCase() + str.slice(1)) ||
    str
      .replace(
        /([\p{L}\d])([\p{L}\d]*)/giu,
        (g0, g1, g2) => g1.toUpperCase() + g2.toLowerCase()
      )
      .replace(/[^\p{L}\d]/giu, "");

  return pascalCache[str];
}

const knownLayouts = new Set();
const knownNotLayouts = new Set();

export default function defaultLayoutPlugin() {
  return function (tree, file) {
    try {
      // only try to apply a layout if one is not already specified
      const hasLayout = file?.data?.astro?.frontmatter?.layout;
      if (hasLayout) {
        return;
      }

      const filePathFull = file.history[0].replace(/\/[^\/]+$/, "");
      const nestedDirs = filePathFull.slice(
        join(file.cwd, pagesPath).length + 1
      );
      const directories = nestedDirs ? nestedDirs.split("/").reverse() : [];
      let layout = defaultLayout;
      directories.some((directory) => {
        const layoutName = toPascalCase(directory);
        if (
          knownLayouts.has(layoutName) ||
          (!knownNotLayouts.has(layoutName) &&
            existsSync(join(layoutPath, layoutName + ".astro")))
        ) {
          knownLayouts.add(layoutName);
          layout = layoutName;
          return true;
        } else {
          knownNotLayouts.add(layoutName);
        }
      });
      file.data.astro.frontmatter.layout = `${layoutAlias}/${layout}.astro`;
    } catch (e) {
      console.error("Couldn't apply default layout", e);
    }
  };
}
