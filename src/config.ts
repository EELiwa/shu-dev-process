export const SITE = {
  title: "SHU Development Process",
  shortTitle: "SHU Dev",
  description:
    "A course and language agnostic set of resources for you to use as you develop software.",
  defaultLanguage: "en_US",
};

export const OPEN_GRAPH = {
  image: {
    src: "/icons/social-share.jpg",
    alt: "SHU Development Process logo on a pink background",
  },
  twitter: "",
};

// This is the type of the frontmatter you put in the docs markdown files.
export type Frontmatter = {
  title: string;
  description?: string;
  furtherReading?: string;
  layout?: "@MainLayout" | string;
  image?: { src: string; alt: string };
  dir?: "ltr" | "rtl";
  ogLocale?: string;
  lang?: string;
  draft?: boolean;
};

export const KNOWN_LANGUAGES = {
  English: "en",
} as const;
export const KNOWN_LANGUAGE_CODES = Object.values(KNOWN_LANGUAGES);

export const COMMUNITY_INVITE_URL = `https://codeberg.org/aserg/shu-dev-process`;

//myhallam is the landing page for the uni, so we should stick to this alone in order to avoid broken links
// or sending students to the wrong place
export const EXTRA_LINKS = [
  {
    text: "MyHallam",
    link: "https://www.shu.ac.uk/myhallam/",
  },
];

export const BUILD_STR = `${new Date().getTime()}`.replaceAll(/[^0-9]+/gm, "");

//If an entry in the sidebar does not have a 'link' attribute or it explicity flagged as as such, it will appear as a subheading
export type Sidebar = Record<
  (typeof KNOWN_LANGUAGE_CODES)[number],
  Record<string, { text: string; link?: string; subheading?: boolean }[]>
>;

export const FILTER_OPTIONS = {
  label: "Levels",
  prefix: "Level",
  multiple: false,
  hideByDefault: false,
  items: ["4", "5", "6"],
};

export const SIDEBAR: Sidebar = {
  en: {
    Initiation: [{ text: "Index", link: "en/initiation" }],
    Planning: [
      { text: "Communication", link: "en/planning/communication/" },
      { text: "SMART Goals", link: "en/planning/smart-goals/" },
      { text: "Project Management", link: "en/planning/project-management/" },
      {text:"Version Control", subheading: true},
      {
        text: "Commits, Branches, & Workflows",
        link: "en/planning/version-control/",
      },
      { text: "IDE Tools", link: "en/planning/version-control/ide-tools/" },
      {
        text: "GitHub Actions",
        link: "en/planning/version-control/github-actions/",
      },
    ],
    Modelling: [
      { text: "Introduction", link: "en/modelling" },
      { text: "Analysis", subheading: true },
      {
        text: "User Stories & Personas",
        link: "en/modelling/analysis/user-stories-and-personas/",
      },
      { text: "Use Cases", link: "en/modelling/analysis/use-case-guidance/" },
      {
        text: "MoSCoW Analysis",
        link: "en/modelling/analysis/moscow-analysis/",
      },
      { text: "Design", subheading: true },
      // { text: "Introduction", link: "en/modelling/design"},
      { text: "Class Diagrams", link: "en/modelling/design/class-diagrams/" },
      {
        text: "Entity Relationship Diagrams",
        link: "en/modelling/design/entity-relationship-diagrams/",
      },
      {
        text: "Sequence Diagrams",
        link: "en/modelling/design/sequence-diagrams/",
      },
      {
        text: "Architecture Overview",
        link: "en/modelling/design/architecture-overview/",
      },
    ],
    Construction: [
      { text: "Index", link: "en/construction" },
      { text: "Comments", link: "en/construction/comments/" },
      { text: "Review", link: "en/construction/review/" },
      { text: "Testing", link: "en/construction/testing/", subheading: true },
      { text: "Unit Testing", link: "en/construction/testing/unit-testing/" },
      {
        text: "Integration Testing",
        link: "en/construction/testing/integration-testing/",
      },
      {
        text: "User Acceptance Testing",
        link: "en/construction/testing/user-acceptance-testing/",
      },
      {
        text: "Automating Testing",
        link: "en/construction/testing/automating-tests/",
      },
    ],
    Deployment: [
      { text: "Delivery", link: "en/deployment/delivery/" },
      {
        text: "Support & Feedback",
        link: "en/deployment/support-and-feedback/",
      },
    ],
    Site: [
      { text: "About", link: "en/site/" },
      { text: "Contributing", link: "en/site/#contributing" },
      { text: "License", link: "en/site/#license" },
      { text: "Privacy", link: "en/site/#privacy" },
      { text: "Team", link: "en/site/#team" },
    ],
  },
};
