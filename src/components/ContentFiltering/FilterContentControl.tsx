import React from "react";
import type { FunctionalComponent } from "preact";
import { FILTER_OPTIONS } from "@config";

let NAME = FILTER_OPTIONS?.label || FILTER_OPTIONS?.prefix || "Filter";

const filterIcon = (
    <svg
        aria-hidden="true"
        focusable="false"
        role="img"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 512 512"
        height="1.2em"
        width="1.2em"
        // style="vertical-align:middle;margin-right:0.5em;"
        className="content-filter-icon"
    >
        <path
            fill="currentColor"
            d="M3.853 54.87C10.47 40.9 24.54 32 40 32H472C487.5 32 501.5 40.9 508.1 54.87C514.8 68.84 512.7 85.37 502.1 97.33L320 320.9V448C320 460.1 313.2 471.2 302.3 476.6C291.5 482 278.5 480.9 268.8 473.6L204.8 425.6C196.7 419.6 192 410.1 192 400V320.9L9.042 97.33C-.745 85.37-2.765 68.84 3.854 54.87L3.853 54.87z"
        />
    </svg>
);

const FilterContentControl: FunctionalComponent<{ lang: string }> = ({ lang = "en" }) => {
    const stored = localStorage.getItem("content-filter");
    const ADF_NAME = stored
        ? (FILTER_OPTIONS.prefix ? FILTER_OPTIONS.prefix + " " : "") + stored
        : NAME;
    const allOptions = FILTER_OPTIONS.items.join(",");
    return (
        <div className="content-filter-select-wrapper" title="Filter displayed content">
            {FILTER_OPTIONS.multiple && (
                <details className="content-filter-select">
                    <summary title={ADF_NAME+" filter control"}>
                        {filterIcon} Viewing content for...&nbsp;
                        <span className="content-filter-multi-name">{ADF_NAME || "Filter"}</span>
                    </summary>
                    <ul>
                        {FILTER_OPTIONS.items.map((opt, idx) => (
                            <li key={idx.toString()} title={opt}>
                                <input type="checkbox" id={`content-filter-${idx}`} value={opt} />
                                <label htmlFor={`content-filter-${idx}`}>
                                    {FILTER_OPTIONS.prefix.length >= 1 &&
                                        FILTER_OPTIONS.prefix + " "}
                                    {opt}
                                </label>
                            </li>
                        ))}
                    </ul>
                </details>
            )}
            {!FILTER_OPTIONS.multiple && (
                <div>
                    {filterIcon} Viewing content for:&nbsp;
                    <select
                        className="content-filter-select"
                        placeholder={NAME || "Filter"}
                        multiple={FILTER_OPTIONS.multiple ? true : false}
                        aria-label={NAME+" filter"}
                    >
                        {NAME && (
                            <option value="">
                                <span>{FILTER_OPTIONS.hideByDefault ? "" : "All "}{FILTER_OPTIONS.label || FILTER_OPTIONS.prefix}</span>
                            </option>
                        )}
                        {!NAME && (
                            <option value="">
                                <span>No Filter</span>
                            </option>
                        )}
                        {FILTER_OPTIONS.items.map((opt,idx) => {
                            return (
                                <option value={opt} key={idx}>
                                    <span>
                                        {FILTER_OPTIONS.prefix && FILTER_OPTIONS.prefix + " "}
                                        {opt}
                                    </span>
                                </option>
                            );
                        })}
                    </select>
                </div>
            )}
        </div>
    );
};

export default FilterContentControl;
