import React from "react";
import type { FunctionalComponent } from "preact";
import { EXTRA_LINKS } from "@config";

const ExtraLinks: FunctionalComponent<{ lang: string }> = ({ lang = "en" }) => {
    return (<>
        {EXTRA_LINKS?.length && (<><details open className="arrow" title="Elsewhere at SHU">
            <summary><h2 className="heading no-anchor">Elsewhere at SHU</h2></summary>
                <ul>
                    {EXTRA_LINKS.map((item, idx) => (
                        <li key={idx.toString()} title={item.text} className={`header-link depth-2`}>
                            <a href={item.link}>
                                <span className="fas fa-comment-alt"></span>
                                {item.text}
                            </a>
                        </li>
                    ))}
                </ul>
        </details>
        </>)}
    </>
    )
};
export default ExtraLinks;