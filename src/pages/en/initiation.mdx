---
title: Initiation
description: The aim of this document is to give you an overview of how you go about understanding your requirements, what sort of requirements you need to think about and how meeting minutes are created for any meetings you undertake.
furtherReading: "Take a look at Software Bill of Materials (SBOMs) if your project needs to be licensed correctly."
---
import FilterContent from "@contentFilter";

The Initiation phase (sometimes referred to as the Initial Communication phase), is dedicated to understanding the objectives of stakeholders, and for the design and collection of requirements that help identify the expected software functionalities and quality attributes. The aim of this document is to give you an overview of how you go about understanding your requirements, what sort of requirements you need to think about and how meeting minutes are created for any meetings you undertake. There will also be some guidance on a questioning style known as Socratic questioning which can be found in section [4](#4-socratic-questioning-technique).

The following areas will be covered, and these are what you need to do during the initiation phase:

- Understand the initial set of requirements in terms of desired functionality/attributes.
  - Meet the client
  - Read the specification
- Understanding the problem.
  - Define an initial set of requirements for the solution.
  - Identify what you understand
  - Identify things on which you need clarity  
    - What is the problem
    - Form it as a useful question - Socratic questioning?  
- The expected outputs are the initial set of functions/attributes of the system.

## Understand the Requirements

The requirements define what the system should do and how it should do it. It is important to first take a step back and think about the whole picture. What do you want your software to be able to do? The specifications help to define this, and understanding the expected direction and features from this document is extremely important when starting out. A software requirements specification is a document that describes what the software will do and how it will be expected to perform. It also describes the functionality the product needs to fulfill all stakeholders (business, users) needs.  

<FilterContent options="6">
The process of understanding the initial requirements is known in some places as the Requirements Engineering Process which spans 4 subsections. It is the process of defining, ducumenting and maintaining your requirements, and is just a way of organising what you have for later use.
</FilterContent>

### Meeting the Client

Meeting with your client will be the first step in the process of Initiation. This is where the client will talk to you about what they want to develop, and they will want you to follow their requirements. These will be presented to you in the form of a specification document.

### Software Specification

At the initial stage, you will have been given a set of requirements now in the form of the specification. This will define the expectation you need to follow in terms of how the system will perform, the inputs and outputs that are expected, what the program should do and who will be making use of it. It is important now for you to **read** this specification and start to think about the next steps you want to take. Don't worry if you are not sure of what to do at this point, once you've read the specification, this document will help guide you through the next stages you need to think about going forward.

### Identification Stage

Once you have identified all of your requirements, you now need to.

- Identify what you understand
- Identify things on which you need clarity  

With this, you will be able to understand what you need to get clarity on and what you are happy with in terms of your understanding. At this stage you will have had your initial meeting with your client, but once you have understood and wrote down a set of requirements, it is a good idea to meet them again with a set of question you can use to obtain further understanding. To do this you need to think about what the problem is, and think about what kind of questions you need to ask to help you get clarity here.

## Socratic Questioning Technique

When you have to ask questions about something, there is a technique that you can consider when thinking about how to format the questions you ask, so you can get as much information as possible. The key to extracting relevant information is all about how you structure your questions. This section will cover a technique known as Socratic Questioning which will help give you a guide as to how you can go about formatting your questions.  

The socratic approach to questioning is based on the practice of disciplined, thoughtful dialogue.
The Intel Teach Program ([1]), teaches that disciplined practice and thoughtful questioning enable you to examine ideas logically and to determine the validity of those ideas.
This technique is an effective way to explore ideas in depth.
By using this technique, it promotes independant thinking and gives ownership of what you are learning.
Higher level thinking is present when you are thinking, evaluating, debating, discussing, and analysing content using your
own thinking process. These types of questions take practice since in some cases the structure promotes an entirely new approach.

Some tips:

- Plan significant questions that provide meaning and direction to the dialogue
- Use wait time; Allow some time for the person you are questioning to respond
- Follow up on the responses you receive
- Ask probing questions
- Periodically summarise in writing key points that have been discussed  

### Six Types of Socratic Questions

There are six types of socratic questions you can ask, which will solely depend on what you are asking the questions for, and what information you are aiming to extract. The six types along with some example questions you could use that match these types are.

- Clarification
  - Why do you say that?
  - How does this relate to the discussion?
  - "Are you going to include diffusion in your mole balance equations?"
- Probe Assumptions
  - What could we assume then?
  - How can you verify or dissaprove that assumption?
- Probe reasons and evidence
  - What would be an example?
  - What do you think causes this to happen? Why?
  - Do you think that the software is a cause for these issues in deployment?
- Viewpoints and Perspectives
  - What would be an alernative?
  - Why is this the best approach?
  - What are the strengths and weaknesses of ...?
  - How are ... and ... similar?
  - Would you explain why this is beneficial or neccessary? Who benefits?
- Probe implications and consequences
  - What are the consequences of that assumption?
  - What are you implying?
  - How could our results be affected if the software crashes?
- About the question
  - What was the point of this question?
  - What does ... mean?
  - How does ... apply to user requirements?
  - Why do you think I asked this question?

It is important to know what you want to find out so you can ask the right questions about the right thing. The example questions above should give you some insight into what you need ask, dependant on what type of information you are aiming to find out.

## Meeting Minutes

Meeting Minutes, also sometimes referred to as **minutes of a meeting** are the official summary of what happened during a meeting. Any correspondence you have with a client needs to be recorded so that you can look back at them and use them to form your agenda, deliverables and aims to meet before you need to have another meeting. The importance of these is not as key at Level 4, but it is good to get good practice in with these as they are a key artefact for further Levels. At Levels 5 and 6, you are expected to produce meeting minutes for any meetings you have with your clients.

This section will give you a brief look at meeting minutes and include tips to help you write good minutes. A template will also be included for you to make use of, but if you prefer, there are plenty of other templates online. For the most part, the minutes you will write for your meeting in university will be **informal**, but there might be times you need more **formal** minutes (possibly in work involving clients). This won't be covered here but, there is a [template available](/shu-dev-process/files/initiation/Formal-meeting-minutes-template.docx) that you could use to do this which shows what to note down. Informal meeting minutes mainly form a meeting summary, which is mainly what you require at Level 4.  

### How to Write Meeting Minutes

- Be Concise  

The key to writing good meeting minutes is to write concisely. Think of yourself as a journalist who is carefully documenting what is happening at the meeting.

- Be Clear  

There ia a balancing act when it comes to writing meeting minutes. You must keep minutes concise but they also need to be clear so as to provide the necessary information to those who didn't attend the meeting.

- Note the facts

Avoid jotting down personal observations and irrelevant conversations into the minutes. If you want to take seperate notes of your own, you are welcome to do so, but the meeting minutes should be a factual record of the meeting.

**Shorthand tips to write meetings minutes faster**  
When writing the meeting minutes, here are a few tips to help you create your notes much faster.

- Use initials instead of people's full names
- Use acronyms where you can without sacrifcing clarity
- Using sentence fragments is fine as long as it still makes sense.  

A template to help you get started: [meeting check-in template](/shu-dev-process/files/initiation/L4%20Project%20Check-In%20Meeting%20Template.docx).

## References

- [1] Intel Teach Program: The Socratic Questioning Technique - https://www.intel.com/content/dam/www/program/education/us/en/documents/project-design/strategies/dep-question-socratic.pdf
- [2] The Six Types of Socratic Questions- http://problemsolving.engin.umich.edu/strategy/cthinking.htm)
- [3] QRACorp Func and Non-Func Requirements - https://qracorp.com/guides_checklists/functional-vs-non-functional-requirements/
- [4] Hugo: Meetings Minutes - https://www.hugo.team/blog/meeting-minutes-with-samples-templates
