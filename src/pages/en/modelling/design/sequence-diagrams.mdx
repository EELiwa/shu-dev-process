---
title: Sequence Diagrams

description: A diagram that describes the interactions among classes in terms of an exchange of messages over time. These can help to predict how a system will behave and to discover responsibilities a class may need.
---
import FilterContent from "@contentFilter";

This style of diagram describes the interactions among classes in terms of an exchange of messages over time. They are sometimes known as **event diagrams**. It is a good way to visualise and validate various runtime scenarios. These can help to predict how a system will behave and to discover responsibilities a class may need to have in the process of modeling a new system.

![Example sequence diagram of a hotel reservation system](https://cdn-images.visual-paradigm.com/guide/uml/what-is-sequence-diagram/01-sequence-diagram-example.png)  
_Figure 7: An example Sequence Diagram of a Hotel Reservation System_

## Sequence Diagram Notation

Visual Representations of any notation mentioned here can be found at [[3]](#4-references).

- **Actor**: This is a type of role by an entity that interacts with the subject. It represents roles as human users, external hardware or other subjects. It is important to note that an actor doesn't necessarily represent a specific physical entity but merely a particular role of some entity
- **Lifeline**: A lifeline represents an individual participant in the interaction
- **Call Message**: Call message is a kind of message that represents an invocation of operation of target lifeline
- **Return Message**: Return message is a kind of message that represents the pass of information back to the caller of a corresponded former message
- **Self Message**: Self message is a kind of message that represents the invocation of message of the same lifeline
- **Recursive Message**: Recursive message is a kind of message that represents the invocation of message of the same lifeline. It's target points to an activation on top of the activation where the message was invoked from
- **Create Message**: Create message is a kind of message that represents the instantiation of (target) lifeline
- **Destroy Message**: Destroy message is a kind of message that represents the request of destroying the lifecycle of target lifeline
- **Duration Message**: Duration message shows the distance between two time instants for a message invocation

## Message and Focus of Control

An event is any point in an interaction where something occurs. Focus of control, also called execution occurrence, is shown as a tall, thin rectangle on a lifeline. It represents the period during which an element is performing an operation. The top and the bottom of the rectangle are aligned with the initiation and the completion time respectively.

![Example of message and focus of control](https://cdn-images.visual-paradigm.com/guide/uml/what-is-sequence-diagram/13-message-and-focus-of-control.png)  
_Figure 8: Visual example of message and focus of control_

## Sequence Fragments

UML 2.0 has helped to introduce sequence fragments, which make it easier to create and maintain accurate sequence diagrams. A sequence fragment is represented as a box, called a combined fragment, which encloses a portion of the interactions with a sequence diagram. TThe fragment operator indicates the type of fragment. More information on these types can be found at [[1]](#references).

![A sequence fragment](https://cdn-images.visual-paradigm.com/guide/uml/what-is-sequence-diagram/14-fragment.png)  
_Figure 9: A sequence fragment_

<FilterContent options="5, 6">
    ## Frame

In UML 2, the frame is an element that is used as a basis for many other diagram elements in UML 2, but the first place most people will encounter a frame element is as the graphical boundary of a diagram. In addition to providing the visual border, the frame element also has an important functional use in diagrams that depict interaction, such as the sequence diagram. Incoming and outgoing messages can make use of the frame, seen in figure 8 above, which is done by connecting the messages to the border of the frame.

![The Frame](https://developer.ibm.com/developer/default/articles/the-sequence-diagram/images/3101_figure1.jpg)  
_Figure 9: the Frame_

### Lifelines

A lifeline represents an individual participant in the interaction. Lifeline notation elements are placed across the top of the diagram. They represent either roles or object instances that participate in the sequence being modeled. Lifelines are drawn with a dashed line descending from the centre of the bottom edge. The name of the lifeline is placed inside the box.

![The Lifeline](https://developer.ibm.com/developer/default/articles/the-sequence-diagram/images/figure3.jpg)  
_Figure 3: the Lifeline_

The lifeline in figure 10 represents an instance of the class Student, whose instance name is freshman. The lifeline name here is underlined, which means that the lifeline represents a specific instance of a class in a sequence diagram, and not a particular kind of instance (a role). Sequence diagrams can include roles, even if they don't really specify who holds those roles by name. This allows diagram re-use in different contexts. Instance names are underlined, but role names are not.

<FilterContent options="6" block>

#### Anonymous Instances

A lifeline can also be used to represent an anonymous or unnamed instance. When modeling an unnamed instance on a sequence diagram, the lifeline’s name follows the same pattern as a named instance; but instead of providing an instance name, that portion of the lifeline’s name is left blank.

Figure 3 shows a named object, but not all lifelines represent named objects. If the lifeline is representing an anonymous instance of the Student class, the lifeline would be: ” Student.” Also, because sequence diagrams are used during the design phase of projects, it is completely legitimate to have an object whose type is unspecified: for example, “freshman”.
</FilterContent>

### Messages

Your first message of a sequence diagram always starts at the top, and is typically located in the left side of the diagram for readability. Subsequent messages are then added to the diagram slightly lower than the previous message. If you want to show an object (e.g. lifeline) sending a message to another object, you draw a line to the recieving object with a solid arrowhead, or with a stick, dependant on the signal. This depends on the signal and type of message, which can be seen in figure 12 below.

![The Message](https://developer.ibm.com/developer/default/articles/the-sequence-diagram/images/3101_figure6.jpg)  
_Figure 11: the Message_

The message / method name is placed above the arroved line. The message that is being sent to the recieving object represents an operation / method that the recieving object's class implements. In figure 12, the analyst object makes a call to the system object which is an instance of the ReportingSystem class. The analyst object is calling the system object’s getAvailableReports method. The system object then calls the getSecurityClearance method with the argument of userId on the secSystem object, which is of the class type SecuritySystem. (Note: When reading this sequence diagram, assume that the analyst has already logged into the system.)

![Example message with return messages](https://developer.ibm.com/developer/default/articles/the-sequence-diagram/images/figure4.jpg)  
_Figure 12: an example message, with return messages present_

### Guards

When modeling object interactions, there will be times when a condition must be met for a message to be sent to the object. Guards are used throughout UML diagrams to control flow. In figure 14, the guard is the text "[pastDueBalance = 0]". By having the guard on this message, the addStudent message will only be sent if the accounts recievable system returns a past due balance of zero. The notation for this in UML is pretty simple.

```UML2
[pastDueBalance = 0]
```

![The Guard](https://developer.ibm.com/developer/default/articles/the-sequence-diagram/images/3101_figure7.jpg)  
_Figure 13: the Guard_

### Alternatives

We use alternatives to designate a mutually exclusive choice between two or more message sequences. While it is possible for two or more guard conditions attached to different alternative operands to be true at the same time, but at most only one operand will actually occur at run time. Alternatives allow the modelling of the classic "if then else" logic we have come to know (and maybe even love).

An alternative **combination fragment** eleemnt is drawn using a frame. The word "alt" is placed inside the frame's namebox. The larger rectangle is then divided into what UML 2 calls operands. (Note: Although operands look a lot like lanes on a highway, I specifically did not call them lanes. Swim lanes are a UML notation used on activity diagrams.) Operands are separated by a dashed line. Each operand is given a guard to test against, and this guard is placed towards the top left section of the operand on top of a lifeline. (Note: Usually, the lifeline to which the guard is attached is the lifeline that owns the variable that is included in the guard expression.) If an operand’s guard equates to “true,” then that operand is the operand to follow.

Alternative combination fragments are not limited to simple "if then else" tests. There can be as many alternatives path as are needed (or as you want). If you need more alternatvies, then all you need to do is add an operand to the rectangle with that sequence's guard and messages.

![The Alternative](https://developer.ibm.com/developer/default/articles/the-sequence-diagram/images/3101_figure8.jpg)  
_Figure 14: the Alternative_

All information can be found in further detail at the References below.

</FilterContent>

<FilterContent options="6" block>
## Option

The option cobination fragment is used to model a sequence that, given a certain condition, will occur; otherwise the sequence does not occur. An option is used to model the "if then" logic statement. It is similar to the alternative fragment notation, except that it only has one operand and there can never be an "else" guard. To draw an option, you first draw a frame. The text "opt" is placed inside the frame's namebox and in the frame's content areas the option's guard is placed towards the top left corner on top of a lifeline.

![The Option](https://developer.ibm.com/developer/default/articles/the-sequence-diagram/images/3101_figure9.jpg)  
*Figure 5: the Option*  

### Loops

Sometimes you will need to model a repetitive sequence, which can be done by making use of a loop. The loop combination fragment is very similar in appearance to the option combination fragment. You draw a frame, and in the frame’s namebox the text “loop” is placed. Inside the frame’s content area the loop’s guard is placed towards the top left corner, on top of a lifeline.  

The loop shown in Figure 6 executes until the reportsEnu object’s hasAnotherReport message returns false. The loop in this sequence diagram uses a Boolean test to verify if the loop sequence should be run. To read this diagram, you start at the top, as normal. When you get to the loop combination fragment a test is done to see if the value hasAnotherReport equals true. If the hasAnotherReport value equals true, then the sequence goes into the loop fragment. You can then follow the messages in the loop as you would normally in a sequence diagram.

![The Loop](https://developer.ibm.com/developer/default/articles/the-sequence-diagram/images/figure10.png)  
*Figure 6: the Loop*  

### Gates

Gates can be an easy way to model the passing of information to another sequence diagram. A gate itself is merely a message that is illustrated with one end connected to the sequence diagram's frame edge and other end connected to a lifeline. The example diagram in Figure 7 has an entry gate called getBalance that takes the parameter of accountNumber. The getBalance message is an entry gate, because it is the arrowed line that is connected to the diagram’s frame with the arrowhead connected to a lifeline. The sequence diagram also has an exit gate that returns the balance variable. The exit gate is known, because it’s a return message that is connected from a lifeline to the diagram’s frame with the arrowhead connected to the frame.

![The Gate](https://developer.ibm.com/developer/default/articles/the-sequence-diagram/images/3101_figure15.jpg)  
*Figure 7: the Gate*

### Break

The break combined fragment is almost identical in every way to the **option combination fragment**, with two exceptions:

- 1 - A break's frame has a namebox with the text "break" instead of "option".
- 2 - When a break's message is to be executed, the enclosing interaction's remainder messages will not be executed because the sequence breaks out of the enclosing interaction.
  
In this way, the break is much like the break keyword in a language such as C++ or Java. Breaks are commonly used to model exception handling in a sequence diagram.

![The Break](https://developer.ibm.com/developer/default/articles/the-sequence-diagram/images/3101_figure16.jpg)  
*Figure 8: the Break*

### Parallel

The parallel combination fragment is drawn using a frame, and you place the text “par” in the frame’s namebox. You then break up the frame’s content section into horizontal operands separated by a dashed line. Each operand in the frame represents a thread of execution done in parallel. Figure 9 is not the best example of Parallel in use, but it offers an easy to understand example of a sequence with parallel activities.

![The Parallel](https://developer.ibm.com/developer/default/articles/the-sequence-diagram/images/3101_figure17.jpg)  
*Figure 9: the Parallel*

These concepts can be seen in more detail in the references.
</FilterContent>

## References

- [1] Visual Paradigm: What is Sequence Diagram - https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-sequence-diagram/
- [2] The sequence diagram - IBM Developer https://developer.ibm.com/technologies/web-development/articles/the-sequence-diagram/
- [3] PEDIAA: What is the Difference Between Transactional Data and Operational Data - https://pediaa.com/what-is-the-difference-between-transactional-data-and-operational-data