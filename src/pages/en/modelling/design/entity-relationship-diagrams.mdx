---
title: Entity Relationship Diagrams

---
import FilterContent from "@contentFilter";

An Entity Relationship Diagram is a type of flowchart that illustrates how 'entities' such as people, objects or concepts relate to each other within a system. They are most often used to design or debug **relational databases** within software engineering, business information systems, education and research. Sometimes known as ERDs or ER Models, they use a defined set of symbols such as rectangles, diamonds, ovals and connecting lines to depict the interconnectedness of entities, relationships and their attributes.

![Entity relationship diagram for a school database](images/modelling/erd-school.svg)  
*Figure 6: Entity Relationship Diagram for a School Database*  

## Components & Features

ER Diagrams are composed of **entities**, **relationships** and **attributes**. They also depict **cardinality**, which defines relationships in terms of numbers. Lets go through them.  

### **Entity**  

<p align="center">
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-01.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-02.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-03.svg" width="200" />
</p>  

This is a defineable thing, like a person, object, concept or event - that can have some data stored about it. You should think about entities as nouns. The shape of an entity is typically shown as a **rectangle**. The characteristics you need to know about Entities are.  
  
- **Entity Type:** A group of defineable things, such as athletes or students, whereas the entity would be the specific athlete or student.
- **Entity Set:** This is the same as an entity type, except it is defined at a particular point in time, such as students who enroll into a class. A related term is instance, in which the specific person or car would be an instance of the entity set.  
- **Entity Categories:** Entities are categorised as strong, weak or associative. A **strong entity** can be defined solely by it's own attributes, while a **weak entity** can't. An **associative entity** associates entities (or elements) within an entity set.  
- **Entity Keys:** This refers to an attribute that uniquely defines an entity in an entity set. Entity keys can be **Super**, **Candidate**, or **Primary**.
  - **Super Key:** A set of attributes (one or more) that together define an entity in an entity set
  - **Candidate Key:** A minimal key super key, meaning that it has the least possible number of attributes to still be a super key
  - **Primary Key:** A candidate key chosen by the database designer to uniquely identify the entity set
  - **Foreign Key:** Identifies the relationship between entities

### **Relationship**

<p align = "center">
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-09.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-10.svg" width="200" />
</p>  

This defines how entities act upon each other or how they are associated with one another. Think of them as verbs, e.g. a named student registers for a course, the two entities would be the student and the course, and the relationship between them would be that this student is enrolled on this course, depicted by the act of enrolment. Relationships are typically shown as **diamonds**. A **Recursive Relationship** describes when the same entity participates more than once in the relationship.

### **Attribute**

<p align = "center">
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-04.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-05.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-07.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-08.svg" width="200" />
</p>

An Attribute is defined as a property or characteristic of an entity. This is often shown as an **oval** or **circle**. A **Descriptive Attribute** is a property or characteristic of a relationship (versus of an entity). The categories of an attribute are.

- **Simple:** This means the attribute value is atomic (constant) and can't be further divided, such as a phone number
- **Composite:** Sub-attributes spring from an attribute
- **Derived:** Attributed is calculated or otherwsie derived from another sub attribute, such as age from a birthdate

### **Cardinality**

<p align = "center">
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-11.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-12.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-15.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-16.svg" width="200" />
</p>

This defines the numerical attributes  of the relationship between two entities or entity sets. The three main cardinal relationships are.

- **one-to-one**: e.g. a student associated with one mailing list
- **one-to-many**: e.g. One student registers for multiple courses, but all those courses have a single line back to that one student
- **many-to-many**: e.g. Students as a group are associated with multiple faculty members, and faculty members in turn are associated with multiple students

Cardinality can be shown as look-across or same-side, depending on where the symbols are shown. The minimum or maximum numbers that apply to a relationship. More information on ER diagrams can be found in the LucidChart references.

<FilterContent options="5,6" block>

## Entity Notations and Attributes

There are primarily four cases where you would want to use an Entity Relationship Diagram during design, which are;

- Database design
- Database debugging
- Database creation and patching
- Aid in requirmement gathering

ER Diagrams are composed of **entities**, **relationships** and **attributes**. They also depict **cardinality**, which defines relationships in terms of numbers. As we went through these in level 4, this time we are going to focus on hese with more detailed examples to help you shape your own work.

### **Entity**  

Entities are defineable objects that can have some data stored about it. You should think about entities as nouns. The shape of an entity is typically shown as a **rectangle**.

### **Entity Attributes**

Also known as a column, an attribute is a property or characteristic of the entity that holds it. The attribute has a name that helps to describe the property and a type that describes the kind of attribute it is, such as **varchar** for a string, or **int** for an integer.

![Entity with properties example](https://cdn-images.visual-paradigm.com/guide/data-modeling/what-is-erd/04-an-erd-entity-with-entities.png)  
*Figure 4: Example Entity with properties*  

### Primary Key

Sometimes known as PK, the primary key is a special kind of entity attribute that is used to **uniquely define** a record in a database table . For example, if we have a unique ID for each purchase in a database, but one of the transactions has the same ID as another transaction, and this transaction ID is our PK, then this is an error and shouldn't be possible, as our PK **must** always be unique. Lets look at a visual example of this:  

![Example of a primary key](https://cdn-images.visual-paradigm.com/guide/data-modeling/what-is-erd/05-concept-of-erd-primary-key.png)  
*Figure 5: Primary Key example*  

As we mentioned before, this type of ID cannot have a duplicate, so this would be an error that we would need to fix within the database.

### Foreign Key

Sometimes known as FK, the foreign key is a **reference** to a primary key in **another table**. It is used as a way to identify the relationships between entities. Note that foreign keys don't need to be unique, unlike primary keys. Multiple records can share the same values. The example below shows an entity with some columns, showing where a reference might take place.  

![Example of a foreign key](https://cdn-images.visual-paradigm.com/guide/data-modeling/what-is-erd/06-concept-of-erd-foreign-key.png)  
*Figure 6: Foreign Key example*  

### ER Relationships

Remember that relationships help to signify the connection between two entities that are associated with each other in some way.

### ER Cardinality

This is the definition that defines the numerical attributes between two entities or entity sets. Recall we we have the three main caridnal relationships of **one-to-one**, **one-to-many** and **many-to-many**.

- **One-to-one**: A one-to-one relationship is mostly used to split an entity in two to provide information concisely and make it more understandable, the example below shows this in practice:  
![One-to-one relationship](https://cdn-images.visual-paradigm.com/guide/data-modeling/what-is-erd/07-erd-one-to-one-relationship-example.png)  
- **One-to-many**: A one-to-many relationship refers to the relationship between two entities X and Y in which an instance of X may be linked to many instances of Y, but an instance of Y is only linked to one instance of X  
![One-to-many relationship](https://cdn-images.visual-paradigm.com/guide/data-modeling/what-is-erd/08-erd-one-to-many-example.png)  
- **Many-to-many**: Many-to-many refers to the relationship between two entities X and Y in which X may be linked to many instances of Y and vice versa. Note that a many-to-many relationship is split into a pair of one-to-many relationships in a physical ERD.  
![Many-to-many relationship](https://cdn-images.visual-paradigm.com/guide/data-modeling/what-is-erd/09-erd-many-to-many-example.png)  

### Example ER Diagram

The diagram below shows an example of a good, simple ER diagram showing the *Physical Data Model*. The Physical Data Model represents the *actual design of a relational database*. At Level 5, we are expecting you to create good quality ER diagrams, similar to the one below:
  
![Example ER diagram](https://cdn-images.visual-paradigm.com/guide/data-modeling/what-is-erd/12-physical-data-model-example.png)  
*Figure 7: Physical Data Model example ER Diagram*
</FilterContent>

<FilterContent options="6" block>
## Data Models

There are three types of conceptual models when we think about ERDs. They differ in the purposes they are created for and for the audiences they are intended for. At university, you will likely only encounter the **Physical Model**, but it is useful at Level 6 to know about the other two types: the **Conceptual Model** and the **Logical Model**. You can find more in depth information about these models at [[1]](#4-references).

### Physical Model

The physical model represents the *actual design of a relational database*. A physical data model elaborates on the logical data model by assigning each column with type, length, nullable etc. Since this kind of model represents how data should be structured and related in a specific DBMS (DataBase Management System), it is importnat to consider the convention and restriction of the actual database system in which the database will be created. It is important to make sure that the column types are supported by your DBMS and reserved words are not used in naming entities and columms.
  
![Physical data model example](https://cdn-images.visual-paradigm.com/guide/data-modeling/what-is-erd/12-physical-data-model-example.png)  
*Figure 1: Physical Data Model example*  

Below is another, more detailed example of a physical model showing a movie rental system:

![Example ERD of a movie rental system](https://cdn-images.visual-paradigm.com/guide/data-modeling/what-is-erd/13-erd-example-movie-rental-system.png)  
*Figure 2: ERD Example - Movie Rental System*

### Conceptual Model

Conceptual ERD models the *business objects that should exist in a system and the relationships between them*. This type of model is developed to present an overview of the entire system by recognizing the business objects involved as well. It defines what entities exist, **NOT** which tables. A *many to many* table may exist in a logical or physical model, but they are just shown as a relationship with no cardinality in the conceptional model.

An example of a conceptual data model can be seen below:

![Conceptual data model example](https://cdn-images.visual-paradigm.com/guide/data-modeling/what-is-erd/10-conceptual-data-model-example.png)  
*Figure 3: Conceptual Data Model example*  

One interesting feature of the conceptual model is that it is the only data model that supports *generalization*. This is because it is used to model a 'kind of' relationship, such as a triangle being a kind of shape. This is the only model that can do this.

### Logical Model

Logical ERD is essentially just a detailed version of a Conceptual ERD. It is developed to enrich a conceptual model by defining explicitly the columns in each entity and introducing *operational* and *transactional* entities. The main difference between transactional data and operational data is that transactional data is the data that describes business events of the organization while operational data is the data that is used to manage the information and technology assets of the organization [3].

An example of a logical data model can be seen below:

![Logical data model example](https://cdn-images.visual-paradigm.com/guide/data-modeling/what-is-erd/11-logical-data-model-example.png)  
*Figure 4: Logical Data Model example*  
</FilterContent>

## References

- [1] Lucidchart: What is an Entity Relationship Diagram? - https://www.lucidchart.com/pages/er-diagrams
- [2] Visual Paradigm What is Entity Relationship Diagram - https://www.visual-paradigm.com/guide/data-modeling/what-is-entity-relationship-diagram
- [3] PEDIAA: What is the Difference Between Transactional Data and Operational Data - https://pediaa.com/what-is-the-difference-between-transactional-data-and-operational-data