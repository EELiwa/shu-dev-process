---
title: UML Class Diagrams
description: 
furtherReading: The full UML specification is almost 800 pages long! Check out the documentation to see what goes into a formal modelling specification - https://www.omg.org/spec/UML/.
---
import FilterContent from "@contentFilter";

When designing a system, before you start to implement a bunch of classes, it is good to have a conceptual understanding of the system that you will be creating. The questions you need to think about are:

- What classes do I need?
- What functionality and information will these classes have?
- How will they interact with each other
- Who can see these classes? (`public`?, `protected`?, `private`?)  

This is where class diagrams come in. They are a neat way of vizualising the classes in your system *before* you actually start coding them up. They're a static representation of your system structure.  

![Example class diagram for a banking system](https://miro.medium.com/max/847/1*TYRSuON0vVxy8olllrBVEw.png)  
*Figure 1: An example Class Diagram for a Banking System*  

The Diagram in figure 1 shows an example of a pretty simple Class Diagram, with the aim to give a general idea of how they look. You may be asking yourself, why do we need these diagrams anyway:

1. Planning and Modeling ahead of time makes programming much easier.  
2. Making changes to class diagrams is easy, whereas making changes in your code is not as easy.  
3. Having a design blueprint makes things go by smoother, this design plan helps you analyse and modify the system.  
4. Not a lot of technical / language specific knowledge is required. 

## The Technical Details

We represent classes in a Class Diagram as a box with three compartments. The uppermost is the **class name**, the middle section contains the **class attributes**, and the bottom section contains the **class methods**.

![Example of a class](https://miro.medium.com/max/552/1*vY53qi20_IryBWOBDnCHpg.png)  
*Figure 2: An example of a Class, represented in UML*  

The convention is.

- attribute name: type
- method name: (parameter:type)  

If you want to set a default, you can do so, just like in figure 2, where **balance: Double = 0.0**, which indicates a default is present, otherwise this would be **balance: Double**. If your method parameters don't take in a value, then you can leave them empty, for e.g. **checkBalance()**.  

## Class Visibility  

Class members (attributes and methods) have a specific visibility assigned to them. See figure 3 below for more details on these.

|         Class Visibility      | Icon |                             Who Can See It?                                |
|-------------------------------|------|----------------------------------------------------------------------------|
|            `public`           |   +  | anywhere in the program and may bbe called by any object within the system |
|           `private`           |   -  | the class that defines it                                                 |
|          `protected`          |   #  | (a) the class that defines it or (b) a subclass of that class              |
|          `package`            |   ~  | instances of other classes within the same package                         |  
*Figure 3: Table representing the Visibility of Class members*  

![Class represented in UML with visibility](https://miro.medium.com/max/547/1*TPgpSIX9iP8L9yYHH6CRfA.png)  
*Figure 4: A Class represented in UML, with added Visibility*

## Relationships

There are many different relationships that you can define within your class diagrams. These are;

- Inheritance
- Association
- Dependency
- Aggregation
- Composition
- Realization

<FilterContent options="4" block>
At level 4, you only need to understand **Inheritance**.
</FilterContent>

### Inheritance

This refers to a type of relationship wherein one associated class is a child of another by virtue of assuming the same functionalities of the parent class. The child is a specific type of the parent class and inherits properties from that parent it is inheriting from. A solid line is used to represent Inheritance with an unfilled arrowhead. **Inheritance** indicates the child (subclass) that is considered to a specialized form of the parent (super class) For example:

![Inheritance in a class diagram](https://miro.medium.com/max/847/1*szU8ngrWSXmBNPYReMyK5w.png)  
*Figure 5: Inheritance within a Class Diagram*  

In figure 5 we can see what Inheritance looks like. In this example we have an **Animal** parent class with all public member fields. The arrows represent the direction of Inheritance. Where the arrow connects the child class to the parent class is naturally labeled as **Inherits from**, indicated by the direction of this clear arrow. The child classes in this example not only inherit methods and attributes from the parent, but they also have some of their own unique methods too, such as **quack()** in Duck and **run()** in Zebra.

<FilterContent options="5,6">

### Association

This is a broad term that simply emcompasses just about any logical connection or relationship between classes. We represent Association as a simple straight line between two different classes. It is typical to name our associations using a verb or verb phrase which reflects the real world problem domain. Alongsaide typical Association, There are a couple of other types of Association that can be used, which are;

- Directed Association
  - A directional relationship represented by a line with an arrowhead, where the arrowhead depcicts a container-contained directional flow  
- Reflexive Association
  - This occurs when a class may have multiple functions or responsibilities  

### Dependency

An object of one class might use an object from another class in the code the method. If the object is not stored in any field, then this is considered, and modeled as a dependency relationship. This is a special type of association, similar to reflexive association, that exists between two classes if making changes to one of the classes will cause changes to another. The first class simply depends on the other class.

### Aggregation

This refers to the formation of a particular class as a result of one class being aggregated or built as a collection. In aggregation, the contianed classes are not strongly dependent on the lifecycle of the container. To show aggregation a diagram, draw a line from the parent class to the child class with a diamond shape near the parent class.

### Composition

This is quite similar to aggregation, with the only difference being its key purpose of emphasizing the dependence of the contained class to the lifecycle of the container class. The contained class will be obliterated when the container class is destroyed. To show compsotion on a diagram, use a directional line connecting the two classes, with a filled diamond shape adjacent to the container class and the directional arrow to the contained class.

### Realization

This denotes the implementation of the functionality defined in one class by another class. To show the relationship in UML, a broken line with an unfilled solid arrow is drawn from the class that defines the functionality of the class that implements the function.

### Cardinality  

Cardinality is what refers to the expression in terms of one to one, one to many and many to many.  
  
![Cardinality](https://cdn-images.visual-paradigm.com/guide/uml/uml-class-diagram-tutorial/11-associations-with-different-multiplicies.png)  

## GUI Class Diagram Example

The following example gives a taste of what a class diagram looks like for a GUI system. This diagram highlights all of the key relationships needed and has labels to help you indicate where the correct relationship needs to be used.  
  
![Class diagram of a GUI system](https://cdn-images.visual-paradigm.com/guide/uml/uml-class-diagram-tutorial/18-uml-class-diagram-example-gui.png)  
*Figure 3: A Class Diagram representation of a GUI system*
</FilterContent>

## References

- [1] Medium.com: UML Class Diagrams Step by Step https://medium.com/@smagid_allThings/uml-class-diagrams-tutorial-step-by-step-520fd83b300b
- [2] Creatly UML Class Diagrams Explained wih Examples - https://creately.com/blog/diagrams/class-diagram-relationships/
- [3] Visual Paradigm UML Class Diagram Tutorial - https://www.visual-paradigm.com/guide/uml-unified-modeling-language/uml-class-diagram-tutorial/