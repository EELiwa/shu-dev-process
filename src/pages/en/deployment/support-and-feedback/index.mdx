---
title: Support & Feedback
description: This step contains guidance regarding assessment, reports and presentations which you will encounter during this course, especially at Levels 5 and 6. We also consider evaluation, reporting and reflection as part of this last step. 
---
import FilterContent from "@contentFilter";

This is the final step of the software development lifecycle. This step contains guidance regarding assessment, reports and presentations which you will encounter during this course, especially at Levels 5 and 6. We also consider evaluation, reporting and reflection as part of this last step.

In addition to the information below, some information has been separated by level of study:

<ul>
  <li data-adf-section="4" data-block><a href="./level-4">Level 4 (reflective reports)</a></li>
  <li data-adf-section="5" data-block><a href="./level-5">Level 5 (projects module)</a></li>
  <li data-adf-section="6" data-block><a href="./level-6">Level 6 (includes dissertation tips)</a></li>
</ul>

## Why Evaluation Is Important

Evaluation is an often overlooked phase in the software lifecycle, however completing an evaluation can greatly improve the quality of the next product/project you are a part of.
Some of the benefits of evaluation include:

- Improves source code quality
- Improves communication and team work
- Clarifies outcomes
- Focuses attention of what went well, and what didn't go as well
- Supports ongoing learning
- Influences future developments

## How You Will Be Assessed

Evaluation of your deliverable should not be overlooked. When you submit your deliverable, it is very likely that you will be asked to produce some form of evaluation of the product you submitted. This may take the form of one or more of the following:

- Reflective report
- Presentation
- Walkthrough

<FilterContent options="4" block>
At level 4, the main form of evaluation assessment that you will encounter at Level 4 will be reflective reports. Especially for your larger projects such as the ones in your Projects module.
</FilterContent>

<FilterContent options="4, 5" block>
For your Projects module, you will be assessed on the following criteria (therefore you should include these in your reflective report):

- An assessment of your software development.
- An assessment of software quality.
- An assessment of validation (conformance to requirements) and verification (testing).
- Discussion of the case study in the context of your course
- Reflections and suggestions

Your group assigments and work with clients are some of the most important pieces on your course. They will provide you with real world experience, allowing you to work with a real client on a real project. Some of your work may have the chance of actually being implemented by your client.

Students need to work with clients to capture a set of requirements and prepare themselves for one or two scrum weeks, an intense block week where a standard 9am-5pm office-hours routine is strictly adhered to.
Students are allocated to a client four to six weeks before the scrum week and have a similar interval to the next scrum week.
Constant communication with the client is encouraged and the resulting software is presented to client at the end of the scrum week.

In work with involving clients, a typical assessment would consider three areas:

- Preparation report.
- Scrum week performance and deliverable.
- Post Scrum week reflective report and screencast.

Two of the three aspects above involve writing a report.
We will be focussing on these two over the next few sections.

## Preparation report

Your preparation report will be the first piece of work you will produce that will be assessed. As the wording suggests, this report is written before you begin development and it is designed to assess your preparedness for the first Scrum week.
You may think that this initial report will not greatly contribute to the project, however you will find that if you produce a high quality preparation report, you will identify and solve some key issues before they even become issues at all. This will result in a much smoother development experience.

In this report, you should address the following:

- Project specification critique.
- Software development process and supporting tools.
- Resource management.

##  Post Scrum Week Reflective Report

This report will be conducted after the Scrum week. It is usually accompained of a short screencast.
In this report, you will need to demonstrate your contribution and reflect on your performance on the Scrum week.

You should discuss the following in this report:

- Software development process and tools.
- Critique of the application.
</FilterContent>

<FilterContent options="6" block>
At Level 6, we are expecting high quality written reports, presentations and walkthroughs:

Your reports, and presentations should be:

- Well structured and flow well, with as little spelling and gramatical errors as possible.
- They should include most if not all of the aspects covered in the [README file](../README.md) as well as any others that you deem appropriate.
- It should also not be 'bloated' by anything coverd in the 'What not to Include' section, unless absolutely neccessary, such as to discuss a rather complicated section of code you are particularly proud of.

You should focus in particular on four important aspects:

- What went well?
- What didn't go well and why?
- What could be improved in future releases and how would you achieve this?
- How well does your end product meet the initial set of requirements you identified?
</FilterContent>

## What You Should Evaluate

When undertaking your evaluation using any of the above, you should consider the following aspects:

- The solution:
  - How complete is it?
  - Does it work?
  - Is it maintainable?
  - Are you pleased with it?
  - Did you work well?
  - What problems did you encounter?
- User feedback
- Reflect and identify learning
- Refer back to:
  - Specification
  - The requirements you identified
  - Design decisions/models
  - Testing
- Your team:
  - Did you work well?
  - Did you encounter any problems?

There are three main aspects that you should consider when undertaking evaluation through either a report, presentation or walkthrough:

- Structure
- What to Include
- What not to Include

## Reports

A report is likely to be the most common form of evaluation/feedback assessment that you will encounter on this course. It is important to know what makes a good report, and the aspects that you should consider when writing your report.

### Structure

A good report not only relies on what goes into it, it is also important to think about the structure of it.
Your report should be a joy to read, and should captivate the reader from the first couple of sections.
It should follow a clean, and well structured approach, with as little grammatical errors as possible.
The following are a selection of headers that you can cover in your report.
Your report does not neccessarily have to follow this exact structure, but it should give you some idea for what the structure of your project should look like:

- Abstract
- Introduction
- Requirements
- Design
- Implementation and Testing
- Evaluation
- References
- Appendices:
  - Screenshots
  - Design Documents

### What to Include

Along with a solid, well built structure, you also need to know what you should include in your report. This should go hand in hand with the structure, and you should not stray away from your structure while writing your report.
Thinking about what to include in your report will be important in ensuring that you are on the right track with your development, and will assist you reaching the minimum word count. Here are some important aspects that you should consider including in your report:

- Quality of the deliverable.
- Assessment of your chosen methodologies.
- Validation (refer to requirements, models, testing and user feedback).
- Discuss the case study in the context of your course (how you used knowledge you gained from the module and other modules).
- How well you worked as a team.
- Explanations of why you did things.
- Your development process (design, implementation, testing etc).
- Your reflections and learning:
  - Your main achievements (what went well).
  - What could have been done better.
- A management-level overview of your work.

### What Not to Include

As well as what to include, it is also important to consider what you do not need to include in a report. This will reduce 'bloating' your report with unneccessary content which will keep your word count down and your report more concise. It will also ensure that you do not get side-tracked and begin talking about stuff that you do not need to as well as hemp keep your readers attention.
Here are two important details **NOT** to include in your report:

- Source code
- Details of every iteration that you went through

Source code may be acceptable if you are showing off or explaining a particular piece of code that you are proud off, however if you choose to do this, make sure that:

- If showing a code screenshot, make sure you are not using a 'dark' theme in the code editor, this takes the readers' eye towards the image rather than the actual explanation about why you are showing it
- If you copy the code directly, only copy a very small section of it, as it will bloat your report with unneccessary extra words. Also make sure it is formatted well, or else it will look messy.

## Presentations

Presentations are another popular form of assessment, you will likely have to do a few of these throughout your course.
Presentations are usually created on PowerPoint or similar software, then presented as an individual or group to a client.

### Structure

As with a written report, is is important to have a clear structure and flow to your presentations. A presentation that does not flow well will likely be hard to present, and will sound messy and ill-considered. As guidance, The following structure aspects could be considered when creating/presenting a presentation. You do not have to use this structure exactly, but it should give you an idea of what we are looking for:

- Brief introduction to yourselves
- Your understanding of the problem
- Your approach to the problem
- Software architecture
- Data and database
- The design of the interface
- An evaluation of your work (e.g., software testing, experiment, etc)
- A conclusion summarizing what went well and what not, with some indications of future work

### What to Include

A presentation is about showing off your work, and you should aim to do just that. In a written report, a main aspect to include would be an evaluation of what did not go well, this should not be a main focussing point in a presentation, and you are also encouraged to show screenshots and code snippets of work you are particularly proud of. The following are some aspects that should be included in your presentations:

- Explanations
- Screenshots etc
- A high level overview
- What you learned
- What are you most proud of

### What Does not Matter

Students often think that you have to be as professional as possible when presenting a report, such as wearing office clothing. However this is not the case, you are students and are not neccessarily expected to follow the same set of rules of someone working a full-time office job. The following are some aspects students often do, but do not need to be worried about when presenting their presentations:

- Wearing office clothing
- If you get lost or confused
- Being nervous (everyone is and it won't affect your grade)

### Hints and Tips for Presentations

Here are a few hints and tips to help with your presentations:

- Fewer slides.
- More images.
- Fewer words on each slide.
- Don't read the slides.
- Speak slowly and clearly.
- Involve the audience - ask questions.
- If presenting in a group:
  - Let everyone speak.
  - Each prepare your own slides.
  - Use a single template.
  - Don't look bored when your colleagues are speaking.

## Walkthroughs

Along with presentations, you are likely going to have to complete some walkthroughs throughout your course, especially at Levels 5 and Both walkthroughs and presentations can be quite daunting for some students, however, they are nothing to be worried about and give you an opportunity to show off your product, particularly sections you are proud of creating.

Here are some aspects you should think about prior to completing a walkthrough:

- Preparation.
- Showing your work.
- How much detail.
- Answering questions.

Preparation is perhaps the most important aspect in this list. If you prepare well, chances are your walkthrough is going to run smoothly.

For both presentations and walkthroughs, it is ok to be nervous, everyone is, and it won't effect your grade. You also do not have to dress smart, like you would do in an office for example. If you are stuck for what to include in your presentations and reports, refer back to [What You Should Evaluate](#5-what-you-should-evaluate).

### Preparation

You should take some time to prepare for your walkthroughs and demonstrations. Without proper preparation, you will likelt get lost, talk about aspects that you do not need to, or miss important details out.
The walkthrough is about showing off all the functionality of your product, so it is vitally important that you do not miss anything out.
The following should be considered:

- Have confidence
- Is there a script? (not always neccessary)
- Demo to a friend
- The machine (a laptop or a virtual machine in the cloud)

### Hints and Tips for Walkthroughs

The following are some hints and tips for your walkthroughs; some that you may not have thought of previously.

#### The Machine

Think about the following when chosing a machine for your walkthroughs:

- Have all infrastructure
- Install and test your own code
- If using a laptop make sure it is charged
- Use a lower than normal resolution
- Does it connect to SHU wifi?
- Do **NOT** think that you can set it up during the demo

#### Have All Infrastructure

Make sure that you have all of the infrastructure:

- Installed
- Running
- What:
  - Libraries
  - Frameworks
  - Servers

#### Install and Test Your Own Code

- The version that you submitted, even if you have updated it since (not recommended)
- If you have developed your code on your own machines, it is not guarenteed to work on University PC's. So make sure it does in advance.

#### Use a Lower Than Normal Resolution

This is not an obvious one, however it can greatly help your tutor or client read and understand what is going on:

- Tutor will be viewing the screen at an angle
- Tutors want to be able to easily read the screen
- Switch to a more readable mode (i.e don't use a dark theme)

<FilterContent options="6">
## Tips for Writing Your Dissertation

The dissertation with your Final Year Project will be the longest and most important report that you will have to complete on this course. Your dissertation should cover the entire software development lifecycle, from gathering requirements to evaluation and feedback. Although this is longer and involves more detail and research than your average report, the fundamental principles remain the same. To help you with this, we have put together some tips for writing your dissertation (all of these are located in the official module handbook):

- Focus on structure, it is very important to have a well structured dissertation with a good flow. Some chapter headings could be:
  - Abstract
  - Introduction
  - Research
  - Design
  - Development
  - Testing
  - Evaluation
  - Conclusion
- Always check the word limit. For example, if the assessment description states that you should write around 8000 words +- 10%. If you go over 10,000, it is likely you are waffling. The abstract or synopsis should not exceed 300 words.
- Use APA referencing. It is important to find a wide range of references from a variety of sources (books, research papers, websites etc).
- If you are including designs such as Personas and Scenarios or screenshots, include them as appendices (which do not go towards the word count). **Make sure all appendices are referenced at some point in the text.**
- Use 10 point line spacing (1.5 in word), with a font size of 11.
- Use decimal system for numbering sections e.g. (3.2.1).
- Figures should be centred and numbered with a title/caption.
- Pages should be numbered in the bottom-centre of the screen, with page 1 starting at Chapter 1, and Roman numerals being used for the previous pages.

More information on your Final Year Project report can be found in the project/module handbook provided on Blackboard, so make sure to check there if you need any further clarity (encouraged as not everything is covered here). This should be your first point of information.
</FilterContent>